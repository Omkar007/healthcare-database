 /****** Creating Trigger to Update in PatientHistory table on Patient being discharged  *****/
CREATE TRIGGER PatientHistory_UpdateOnDischarge ON Discharge
FOR INSERT AS
BEGIN
    UPDATE PatientHistory
    SET PatientPhysian=INSERTED.EmployeeID,PatientCheckOut=GETDATE(),DischargeID=INSERTED.DischargeID
    FROM 
       PatientHistory
		JOIN INSERTED 
		ON Inserted.PatientID =PatientHistory.PatientID AND PatientHistory.PatientCheckOut IS NULL
END

INSERT INTO Discharge 
VALUES(1,505,'Patient is in good condition.needs visit after 7 days for stiches removal',34)

SELECT * FROM InsuranceCoverage
SELECT * FROM Discharge

/***** Create Function which returns BalanceDue to be paid by Patients afer Innsurance Coverage % is aplied on Total Cost***/
GO
CREATE FUNCTION BalanceDue(@PatientID int)
RETURNS int
BEGIN

RETURN(SELECT ((SELECT TotalCost FROM Billings WHERE PatientID=@PatientID)-(
SELECT ((SELECT SUM(MedicationCost) FROM Medication WHERE MedicationID IN 
(SELECT MedicationID FROM PatientMedicationDetails WHERE PatientID = @PatientID)) + 
(SELECT SUM(ProcedureCost) FROM Procedures WHERE ProcedureID IN 
(SELECT ProcedureID FROM PatientProceduresDetails WHERE PatientID = @PatientID))) * 
(SELECT AmountCoveredInfo/100 FROM InsuranceCoverage WHERE InsuranceCoverageID IN 
(SELECT InsuranceCoverageID FROM Patients WHERE PatientID = @PatientID)))))
END;

/* Demonstrating the Total Cost, Insurance Coverage Amount in % for Patient ID 30 */
SELECT * FROM Billings where PatientID =30
SELECT * FROM Patients where PatientID =30
SELECT * FROM InsuranceCoverage WHERE InsuranceCoverageID=20

/* Calling the Above defined Function and Fenerating the balance Amount bill for Patient*/
SELECT b.BillingID ,(p.PatientFirstName+' '+p.PatientMiddleName+' '+p.PatientLastName) AS PatientName,b.items,
b.PaymentMethod, dbo.BalanceDue(30) AS BalanceDue
FROM Patients p, Billings b
WHERE p.PatientID = b.PatientID AND p.PatientID=30;

/****Creating Updatable view to update the room in which patient is kept when he/she needed to be moved 
to Operation theatre/ICU ****/

CREATE VIEW Patient_Rooms
AS
SELECT p.PatientID,p.PatientRoomID,(p.PatientFirstName+' '+p.patientMiddleName+' '+p.PatientLastName) AS PatientName,r.RoomType 
FROM Patients p, PatientRooms r 
WHERE r.RoomID = p.PatientRoomID

/* Initally Patient id 33(James) is kept in Standard Ward. Now after diagnosis of Cardiac Blockage he is moved to Operation
Theatre for Cardiac Surgery hence room to be assigned is of ID 300
 */
SELECT * FROM Patient_Rooms WHERE PatientID=33;
SELECT * FROM PatientRooms 
SELECT * FROM Patients WHERE PatientID=35;

UPDATE Patient_Rooms
SET PatientRoomID=300
WHERE PatientID=33

SELECT * FROM Patient_Rooms WHERE patientID=33

/*****Creating Script to check if the Room to be which patient is being assigned is vacant
if yes then moving patient with ID 35 from (ICU to Standard Ward) is vacant otherwise printing Room is not vacant  ****/
SELECT * FROM PatientRooms 
SELECT * FROM Patients WHERE PatientID=35;

/* Trying to move PatientID 35 to Room ID 100 which is vacant*/
USE HealthCareCenter
DECLARE @RoomStatus varchar(50)
DECLARE @RoomID int
DECLARE @PatientID int
SET @PatientID = 35
SET @RoomID = 100
SET @RoomStatus = (SELECT RoomStatus FROM PatientRooms WHERE RoomID = @RoomID)

IF @RoomStatus = 'Occupied'
PRINT 'Room '+CONVERT(VARCHAR,@RoomID,1)+' is not vacant.'
ELSE
UPDATE Patients
SET PatientRoomID = @RoomID
WHERE PatientID = @PatientID

/**** Creating a Stored Procedure that checks for Patients entry for first time visit in Patients and 
		then later just creates history in PatientHistory for every new visit  ****/
USE HealthCareCenter
GO
CREATE PROC PatientHistory_Entry
			@PatientID int, @PatientFirstName varchar(40), @PatientMiddleName varchar(40), @PatientHeight varchar(10),
			@PatientWeight varchar(10), @PatientAge int, @Doctor int, @HeartRate varchar(30),
			@BloodPressure varchar(30), @RespiratoryRate varchar(30), @InsuranceCoverage int,@patientAddressID int,
			@RoomID int, @Email varchar(40), @Date varchar(20), @Phone varchar(40)
AS
IF  NOT EXISTS( SELECT * FROM Patients WHERE PatientID = @patientID )
BEGIN
	INSERT INTO Patients 
	VALUES(@PatientID,@PatientFirstName,@PatientMiddleName,NULL,NULL,@Email,@Phone,NULL,NULL,@InsuranceCoverage,@patientAddressID,@Doctor,@RoomID);
	INSERT INTO PatientHistory(PatientWeight,PatientHeight,PatientAge,HeartRate,BloodPressure,RespiratoryRate,PatientCheckIn,PatientCheckOut,DiagnosisID,PatientReferalDoctor,PatientPhysian,PatientID,PatientMedicationInfo,PatientProcedures,HospitalizationInfoID,DischargeID)
	VALUES(@PatientWeight,@PatientHeight,@PatientAge,@HeartRate,@BloodPressure,@RespiratoryRate,convert(datetime,@Date,5),null,null,null,@Doctor,@PatientID,null,null,null,null);
END;

ELSE

BEGIN
	INSERT INTO PatientHistory(PatientWeight,PatientHeight,PatientAge,HeartRate,BloodPressure,RespiratoryRate,PatientCheckIn,PatientCheckOut,DiagnosisID,PatientReferalDoctor,PatientPhysian,PatientID,PatientMedicationInfo,PatientProcedures,HospitalizationInfoID,DischargeID)
	VALUES(@PatientWeight,@PatientHeight,@PatientAge,@HeartRate,@BloodPressure,@RespiratoryRate,convert(datetime,@Date,5),null,null,null,@Doctor,@PatientID,null,null,null,null);
END;
 
 /* Before Calling above created stored procedure to Enter record of new patient with ID 38*/
 SELECT * FROM Patients
 SELECT * FROM PatientHistory

 /* After Calling above created stored procedure to Enter record of new patient with ID 38*/
 EXEC PatientHistory_Entry @PatientID=38, @PatientFirstName ='Kevin', @PatientMiddleName='Mathews', @PatientHeight='6.9',
			@PatientWeight ='79', @PatientAge=28, @Doctor= 501, @HeartRate='69/min',
			@BloodPressure='95-140', @RespiratoryRate ='14/min', @InsuranceCoverage=23,@patientAddressID=6,
			@RoomID=100, @Email='kevin@gmail.com', @Date='07-09-17 07:34:09 PM', @Phone=3149498812; 

/* As shown in result the Patient record is inserted and is history is being created for this visit */
 SELECT * FROM Patients
 SELECT * FROM PatientHistory

 /* Now demonstrating next visit of the patient id 38. As we alreay have the patientinfo we won't be creating
 entry in Patient only his new history is generated (Age, Height and vitals will be taken his new visit) */
 EXEC PatientHistory_Entry @PatientID=38, @PatientFirstName ='Kevin', @PatientMiddleName='Mathews', @PatientHeight='6.9',
			@PatientWeight ='83', @PatientAge=29, @Doctor= 501, @HeartRate='65/min',
			@BloodPressure='138', @RespiratoryRate ='13/min', @InsuranceCoverage=23,@patientAddressID=6,
			@RoomID=100, @Email='kevin@gmail.com', @Date='09-09-17 2:34:09 PM', @Phone=3149498812; 

GO

/* Creating role to provide permissions for insert, update on Patients and PatientHistory table and select for all*/
USE HealthCareCenter 
CREATE ROLE PatientEntry 
GRANT INSERT ON Patients TO PatientEntry 
GRANT UPDATE,INSERT ON PatientHistory TO PatientEntry 
EXEC sp_AddRoleMember db_datareader, PatientEntry 

/* creating new login omkar with username om and assigning the role created above*/
USE HealthCareCenter
CREATE LOGIN Omkar WITH PASSWORD = '657234', 
DEFAULT_DATABASE = HealthCareCenter
CREATE USER Om FOR LOGIN Omkar 
EXEC sp_AddRoleMember PatientEntry, Om 