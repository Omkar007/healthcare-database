/****** Address: Data Entry ******/
SET IDENTITY_INSERT Address ON 
INSERT Address (AddressID,AddressStreetAppartment,State, Country,ZipCode) VALUES 
(1,'450#, 24th Salina Street','NY', 'USA', '13210'),
(4,'A1/200 Hiranandani,Vikhroli','MAH', 'INDIA', '421301'),
(6,'300#, Smith Lane, Columbus Ave','OH', 'USA', '43001'),
(10,'212#, Priceton Street','NJ', 'USA', '07001'),
(15,'312# Folkosm Street, Williamson Ave','CA', 'USA', '90001')
SET IDENTITY_INSERT Address OFF

SELECT * FROM Address

/****** Department: Data Entry *****/

INSERT INTO Department VALUES(1,'Emergency department','New York')
INSERT INTO Department VALUES(2,'Cardiology','New York')
INSERT INTO Department VALUES(3,'Paediatric intensive care unit','Boston')
INSERT INTO Department VALUES(4,'Neonatal intensive care unit','New York')
INSERT INTO Department VALUES(5,'Neurology','New York')
INSERT INTO Department VALUES(6,'Oncology','New York')
INSERT INTO Department VALUES(7,'Cardiovascular intensive care unit','New York')
INSERT INTO Department VALUES(8,'Obstetrics and gynaecology,','New York')
INSERT INTO Department VALUES(9,'General','New York')

SELECT * FROM Department

/****** Pateint Rooms: Data Entry *****/
INSERT INTO PatientRooms VALUES(100,'Standard Ward','1st Floor','4 Beded Room','vacant')
INSERT INTO PatientRooms VALUES(101,'Semi Private','1st Floor','2 Beded Room','Occupied')
INSERT INTO PatientRooms VALUES(201,'Private','2nd Floor','1 Beded Room','vacant')
INSERT INTO PatientRooms VALUES(202,'Standard Ward','2nd Floor','3 Beded Room','Occupied')
INSERT INTO PatientRooms VALUES(301,'Intesive Care Unit','3rd Floor','1 Beded Room','vacant')
INSERT INTO PatientRooms VALUES(300,'Operation theatre','3rd Floor','1 Beded Room','vacant')

SELECT * FROM PatientRooms

/****** Medications: Data Entry *****/
INSERT INTO Medication values(40,'Paracetamol','3/day adults,2/day child','minor aches,pain',10)
INSERT INTO Medication values(41,'Crocin','3/day adult,2/day child','cold/ fever',20)
INSERT INTO Medication values(42,'Peglyated Infereon alfa','2/day adult,1/day child','Hepatatis C',15)
INSERT INTO Medication values(43,'Isoniazoid','2/day adult,1/day child','tuberclosis',25)
INSERT INTO Medication values(44,'Acyclovir','3/day adult,2/day child','cold, soar',31)
INSERT INTO Medication values(45,'Dramamine','2/day adult,1/day child','vomitation',20)
INSERT INTO Medication values(46,'Imodium','2/day adults,1/day child','diarrhea',10)

SELECT *FROM Medication

/****** MedicalDevicesEquipments: Data Entry *****/
INSERT INTO MedicalDevicesEquipments values(1,'Electrocardiograph Channels','X1',2000)
INSERT INTO MedicalDevicesEquipments values(2,'Operationtheatre Spotlights','M7123',3000)
INSERT INTO MedicalDevicesEquipments values(3,'Operating Table','M2423',1200)
INSERT INTO MedicalDevicesEquipments values(4,'Anesthesia trolley','U7243',300)
INSERT INTO MedicalDevicesEquipments values(5,'Suction Pump','I77',300)
INSERT INTO MedicalDevicesEquipments values(6,'Syringe Pump','S55',200)
INSERT INTO MedicalDevicesEquipments values(7,'Operating instrument set','E33',3500)
INSERT INTO MedicalDevicesEquipments values(8,'Ventilator','R12',4000)
INSERT INTO MedicalDevicesEquipments values(9,'Pulsoximeter','T2',2700)
INSERT INTO MedicalDevicesEquipments values(10,'PatientBed','P772',1700)

SELECT * FROM MedicalDevicesEquipments

/****** Insurance Plan: Data Entry *****/
INSERT INTO InsurancePlan values(100,'Health maintenance organizations (HMOs)')
INSERT INTO InsurancePlan values(101,'Preferred provider organizations (PPOs)')
INSERT INTO InsurancePlan values(102,'Exclusive provider organizations (EPOs)')
INSERT INTO InsurancePlan values(103,'Point-of-service (POS) plans')
INSERT INTO InsurancePlan values(104,'High-deductible health plans (HDHPs)')

SELECT * FROM InsurancePlan

/****** Insurance Coverage: Data Entry *****/
INSERT INTO InsuranceCoverage values(20,'Aetna','3129498810','helpdesk@aetna.com',1,2300,'1 yr',70,'Dental & Eye Care',100)
INSERT INTO InsuranceCoverage values(21,'CVS','3159597710','helpdesk@cvs.com',10,1800,'2 yr',80,'Accidental,Surgery & Dental',100)
INSERT INTO InsuranceCoverage values(22,'Cigna','8002446224','helpdesk@cigna.com',15,1700,'1 yr',90,'Eye Care, Accidental & Surgery',101)
INSERT INTO InsuranceCoverage values(23,'Aetna','3129498810','helpdesk@aetna.com',1,1600,'1 yr',70,'Eye Care, Accidental',102)
INSERT INTO InsuranceCoverage values(24,'Molina Health Care','8002005577','helpdesk@Molina.com',10,2100,'1 yr',60,'Eye Care, Accidental,Surgery',100)

SELECT * FROM InsuranceCoverage

/****** Benefits: Data Entry *****/
INSERT INTO Benefits values(400,'Hazard Pay','Employer covers the accidental dmage cause to Employee as long as employee works')
INSERT INTO Benefits values(401,'Maternity,Paternity,Adoption Leave','For women in case of Maternity')
INSERT INTO Benefits values(402,'Paid Holidays','Employee can take paid holidays as decided by company')
INSERT INTO Benefits values(403,'Pay Raise','Employee salary will be revise twice a year')
INSERT INTO Benefits values(404,'Retirement Pay','Pay after employee retires from Company')
INSERT INTO Benefits values(405,'Vacation Leave','Eployee can take vaction and get paid as per company policy')
INSERT INTO Benefits values(406,'Work Breaks and Meal Breaks','Regular free meals and breaks')
INSERT INTO Benefits values(407,'Stock Options','Employee can get share of stocks of company')

SELECT * FROM Benefits

/****** Procedures: Data Entry *****/
INSERT INTO Procedures values(10,'Non Invasive scans',2400,'Non Surgical')
INSERT INTO Procedures values(11,'Body Fluids Test',1500,'Non Surgical')
INSERT INTO Procedures values(12,'Radiation Therapy',30000,'Non Surgical')
INSERT INTO Procedures values(13,'ByPass',20000,'Surgical')
INSERT INTO Procedures values(14,'Endoscopy',15000,'Surgical')
INSERT INTO Procedures values(15,'Rehabitilation',4500,'Non Surgical')

SELECT * FROM Procedures

/****** Shifts: Data Entry *****/
INSERT INTO Shifts values(50,9,'8:00:00','5:00:00','Morning')
INSERT INTO Shifts values(51,9,'11:00:00','8:00:00','Day')
INSERT INTO Shifts values(52,9,'2:00:00','11:00:00','Evening')
INSERT INTO Shifts values(53,9,'11:00:00','8:00:00','Night')

SELECT * FROM Shifts

/****** WorkSchedule: Data Entry *****/
INSERT INTO WorkSchedule values(1001,50,100)
INSERT INTO WorkSchedule values(1002,51,100)
INSERT INTO WorkSchedule values(1003,52,301)
INSERT INTO WorkSchedule values(1004,50,300)
INSERT INTO WorkSchedule values(1005,51,101)
INSERT INTO WorkSchedule values(1006,53,201)
INSERT INTO WorkSchedule values(1007,52,202)

SELECT * FROM WorkSchedule

/****** Employees: Data Entry *****/
INSERT INTO Employees values(501,'Samuel','John','Williams','807356308','samuel@gmail.com','3159498811','110 1st Floor','8777775411',4,1002,1,'Emergency Medicine Specialist','Medicine Specialist (MD)','Permanent','',100000)
INSERT INTO Employees values(502,'Micheal','Fernandes','','807356309','Michael@gmail.com','3159498812','210 2nd Floor','8777775412',6,1004,1,'Emergency Medicine Specialist ','Medicine Specialist (MD)','Permanent','',95000)
INSERT INTO Employees values(503,'Austin','Mitchel','Gabriel','807356310','austin@gmail.com','3159498813','310 3rd Floor','8777775413',10,1003,2,'Cardiovascular Surgeon','Cardiac Physian','Temporary','2',120000)
INSERT INTO Employees values(504,'David','Fincher','','807356311','david@gmail.com','3159498814','112 1st Floor','8777775414',1,1002,9,'Physian','Physian assistant','Permanent','',90000)
INSERT INTO Employees values(505,'Christoper','Adam','Nolan','807356312','christoper@gmail.com','3159498815','211 2nd Floor','8777775415',15,1001,7,'Cardiovascular Surgeon','Cardiac Physian','Permanent','',13000)
INSERT INTO Employees values(506,'James','Clarke','Anderson','807356313','james@gmail.com','3159498816','311 3rd Floor','8777775416',6,1003,5,'Neuro Surgeon','Neurology Physist','Temporary','2',150000)

SELECT * FROM Employees

/****** EmployeeBenefits: Data Entry *****/
INSERT INTO EmployeeBenefits values(401,501)
INSERT INTO EmployeeBenefits values(402,505)
INSERT INTO EmployeeBenefits values(401,506)
INSERT INTO EmployeeBenefits values(405,503)
INSERT INTO EmployeeBenefits values(404,505)
INSERT INTO EmployeeBenefits values(404,506)

SELECT * FROM EmployeeBenefits

/****** RoomMedicalDevices: Data Entry *****/
INSERT INTO RoomMedicalDevices values(100,1)
INSERT INTO RoomMedicalDevices values(101,2)
INSERT INTO RoomMedicalDevices values(201,3)
INSERT INTO RoomMedicalDevices values(202,1)
INSERT INTO RoomMedicalDevices values(300,2)
INSERT INTO RoomMedicalDevices values(301,7)
INSERT INTO RoomMedicalDevices values(300,7)
INSERT INTO RoomMedicalDevices values(201,4)
INSERT INTO RoomMedicalDevices values(300,10)

SELECT * FROM RoomMedicalDevices

/****** Patients: Data Entry *****/
INSERT INTO Patients VALUES(30,'John','Hazard','','806356407','John@gmail.com','907358811','Adria','Hazard',20,1,501,100)
INSERT INTO Patients VALUES(31,'Adan','Mason','Michael','806356408','Adan@gmail.com','907358812','','',21,4,501,101)
INSERT INTO Patients VALUES(32,'Cahil','Phelps','Benjamin','','Cahil@gmail.com','907358813','','',22,6,502,201)
INSERT INTO Patients VALUES(33,'James','Drake','','806356403','James@gmail.com','907358814','','',23,10,502,202)
INSERT INTO Patients VALUES(34,'Moni','Francis','Henderson','','Moni@gmail.com','907358815','Mikel','Anderson',24,15,503,300)
INSERT INTO Patients VALUES(35,'Melina','Austin','Gabriel','806356207','Melina@gmail.com','907358816','','',20,1,503,301)
INSERT INTO Patients VALUES(36,'Martina','Philipines','','806256407','Martina@gmail.com','907358817','','',21,4,504,201)
INSERT INTO Patients VALUES(37,'Francis','Nolan','','','Francis@gmail.com','907358818','Kim','Jeferson',22,6,506,202)
INSERT INTO Patients VALUES(38,'David','Joel','','706356407','David@gmail.com','907358819','','',23,10,505,101)

SELECT * FROM Patients

/****** PatientMedicationDetails: Data Entry *****/
INSERT INTO PatientMedicationDetails VALUES(40,30)
INSERT INTO PatientMedicationDetails VALUES(41,31)
INSERT INTO PatientMedicationDetails VALUES(42,32)
INSERT INTO PatientMedicationDetails VALUES(40,33)
INSERT INTO PatientMedicationDetails VALUES(41,34)
INSERT INTO PatientMedicationDetails VALUES(42,30)
INSERT INTO PatientMedicationDetails VALUES(43,30)
INSERT INTO PatientMedicationDetails VALUES(44,36)

SELECT * FROM PatientMedicationDetails

/****** PatientProceduresDetails: Data Entry *****/
INSERT INTO PatientProceduresDetails VALUES(10,36)
INSERT INTO PatientProceduresDetails VALUES(11,31)
INSERT INTO PatientProceduresDetails VALUES(14,30)
INSERT INTO PatientProceduresDetails VALUES(12,36)
INSERT INTO PatientProceduresDetails VALUES(13,30)
INSERT INTO PatientProceduresDetails VALUES(10,33)
INSERT INTO PatientProceduresDetails VALUES(11,34)
INSERT INTO PatientProceduresDetails VALUES(12,35)

SELECT * FROM PatientProceduresDetails

/****** Visitors Table: Data Entry *****/
INSERT INTO Visitors VALUES(1,'Jonathan Murray','05-07-1992',convert(datetime,'05-06-17 10:34:09 PM',5),convert(datetime,'05-06-17 11:34:09 PM',5),CONVERT(image,'E:\BeyondNoteApp\BeyondNote\app\src\main\res\drawable\icon_user_default.png'),31)
INSERT INTO Visitors VALUES(2,'Neil Ronson','06-06-1990',convert(datetime,'04-05-17 09:34:09 PM',5),convert(datetime,'04-05-17 11:44:09 PM',5),CONVERT(image,'E:\BeyondNoteApp\BeyondNote\app\src\main\res\drawable\icon_camera.png'),30)
INSERT INTO Visitors VALUES(3,'Amy Jackson','07-01-1991',convert(datetime,'09-07-17 11:34:09 PM',5),convert(datetime,'09-07-17 10:34:09 PM',5),CONVERT(image,'E:\BeyondNoteApp\BeyondNote\app\src\main\res\drawable\icon_tasks.png'),31)
INSERT INTO Visitors VALUES(4,'Jack Lewis','05-03-1993',convert(datetime,'03-09-17 10:34:09 PM',5),convert(datetime,'03-09-17 10:34:09 PM',5),CONVERT(image,'E:\BeyondNoteApp\BeyondNote\app\src\main\res\drawable\hashtagimg.png'),32)
INSERT INTO Visitors VALUES(5,'Yun Huang','06-08-1991',convert(datetime,'01-10-17 09:34:09 PM',5),convert(datetime,'01-10-17 10:34:09 PM',5),CONVERT(image,'E:\BeyondNoteApp\BeyondNote\app\src\main\res\drawable\icon_notes.png'),33)
INSERT INTO Visitors VALUES(6,'Yen Cheng','09-05-1989',convert(datetime,'02-08-17 08:34:09 PM',5),convert(datetime,'02-08-17 10:34:09 PM',5),null,34)
INSERT INTO Visitors VALUES(7,'Zen Cheng','10-08-1982',convert(datetime,'08-04-17 06:34:09 PM',5),convert(datetime,'08-04-17 10:34:09 PM',5),CONVERT(image,'E:\BeyondNoteApp\BeyondNote\app\src\main\res\drawable\addcontactsimg.png'),35)

SELECT * FROM Visitors

/****** Diagnosis Table: Data Entry *****/
INSERT INTO Diagnosis VALUES(1,null,'normal','positive',null,'cancer symptoms shown','04-05-2017',37)
INSERT INTO Diagnosis VALUES(2,null,null,null,'chest filled with cough','TB sysptoms shown','06-05-2017',36)
INSERT INTO Diagnosis VALUES(3,'positive','normal',null,null,'diabetes symptoms shown','07-03-2017',33)
INSERT INTO Diagnosis VALUES(4,null,'normal','positive',null,'cancer symptoms shown','08-04-2017',32)
INSERT INTO Diagnosis VALUES(5,'abnormal','abnormal',null,null,'cardiac blockage symptoms shown','02-06-2017',34)
INSERT INTO Diagnosis VALUES(6,'abnormal','abnormal',null,null,'cardiac blockage symptoms shown','01-08-2017',35)

SELECT * FROM Diagnosis

/****** Reviews Table: Data Entry *****/
SET IDENTITY_INSERT Reviews ON 
INSERT Reviews(ReviewID,Review,PatientID,EmployeeID,ReviewDate) VALUES(1,'Not Friendly',30,503,'01-05-2017'),
(2,'Good Doctor. Treated me well!!',35,503,'06-09-2017'),
(3,'Awesome doctor, very friendly',34,505,'07-08-2017'),
(4,'Bad physian. arrogant',31,502,'05-07-2017'),
(5,'Not great but not bad either',36,504,'02-03-2017')
SET IDENTITY_INSERT Reviews OFF

SELECT * FROM Reviews

/****** Discharge Table: Data Entry *****/
INSERT INTO Discharge VALUES(1,505,'Patient is in good condition.needs visit after 7 days for stiches removal',34)
INSERT INTO Discharge VALUES(2,502,'Patient''s condition is good',31)
INSERT INTO Discharge VALUES(3,501,'recovery is fast. needs visist after 5 days for several tests',36)
INSERT INTO Discharge VALUES(4,503,'Patient is in good condition.needs visit after 7 days for stiches removal',35)
INSERT INTO Discharge VALUES(5,505,'revoery is good',33)

SELECT * FROM Discharge

/****** PatientHistory Table: Data Entry *****/
SET IDENTITY_INSERT PatientHistory ON 
INSERT PatientHistory(PatientHistoryID,PatientWeight,PatientHeight,PatientAge,HeartRate,BloodPressure,RespiratoryRate,PatientCheckIn,PatientCheckOut,DiagnosisID,PatientReferalDoctor,PatientPhysian,PatientID,PatientMedicationInfo,PatientProcedures,HospitalizationInfoID,DischargeID) VALUES(1,'74','5.6',23,'65/min','130','13/min',convert(datetime,'05-06-17 10:34:09 PM',5),null,1,504,501,33,43,12,201,1),
(2,'85','6',24,'75/min','120','14/min',convert(datetime,'05-06-17 10:34:09 PM',5),null,5,504,503,34,null,null,301,null),
(3,'67','5.8',26,'85/min','110','12/min',convert(datetime,'05-06-17 10:34:09 PM',5),null,6,504,505,35,null,null,300,null),
(4,'53','5.7',27,'95/min','140','16/min',convert(datetime,'05-06-17 10:34:09 PM',5),null,3,502,502,30,41,11,202,2)
SET IDENTITY_INSERT PatientHistory OFF

SELECT * FROM PatientHistory

/****** RoomShceduleHistory Table: Data Entry *****/

INSERT INTO RoomScheduleHistory VALUES(1,'3','06-12-17',202,'James Drake, Francis Nolan')
INSERT INTO RoomScheduleHistory VALUES(2,'1','06-12-17',201,'Martina Philipines, Cahil Benjamin Philips')
INSERT INTO RoomScheduleHistory VALUES(3,'1','06-12-17',300,(SELECT PatientFirstName+' '+PatientMiddleName+' '+PatientLastName FROM Patients WHERE PatientRoomID IN (300)))

SELECT * FROM RoomScheduleHistory

/****** Billings Table: Data Entry *****/
INSERT INTO Billings VALUES(1,(SELECT COUNT(*) FROM PatientHistory Where PatientID = 33),'credit card','self',((SELECT COUNT(1) FROM PatientMedicationDetails where PatientID = 33)+(SELECT COUNT(1) FROM PatientProceduresDetails where PatientID = 33)),((SELECT SUM(MedicationCost) FROM Medication WHERE MedicationID IN (SELECT MedicationID FROM PatientMedicationDetails WHERE PatientID = 33)) + (SELECT SUM(ProcedureCost) FROM Procedures WHERE ProcedureID IN (SELECT ProcedureID FROM PatientProceduresDetails WHERE PatientID = 33))),33)
INSERT INTO Billings VALUES(2,(SELECT COUNT(*) FROM PatientHistory Where PatientID = 30),'credit card','self + insurance',((SELECT COUNT(1) FROM PatientMedicationDetails where PatientID = 30)+(SELECT COUNT(1) FROM PatientProceduresDetails where PatientID = 30)),((SELECT SUM(MedicationCost) FROM Medication WHERE MedicationID IN (SELECT MedicationID FROM PatientMedicationDetails WHERE PatientID = 30)) + (SELECT SUM(ProcedureCost) FROM Procedures WHERE ProcedureID IN (SELECT ProcedureID FROM PatientProceduresDetails WHERE PatientID = 30))),30)
INSERT INTO Billings VALUES(3,(SELECT COUNT(*) FROM PatientHistory Where PatientID = 34),'credit card','self',((SELECT COUNT(1) FROM PatientMedicationDetails where PatientID = 34)+(SELECT COUNT(1) FROM PatientProceduresDetails where PatientID = 34)),((SELECT SUM(MedicationCost) FROM Medication WHERE MedicationID IN (SELECT MedicationID FROM PatientMedicationDetails WHERE PatientID = 34)) + (SELECT SUM(ProcedureCost) FROM Procedures WHERE ProcedureID IN (SELECT ProcedureID FROM PatientProceduresDetails WHERE PatientID = 34))),34)

SELECT * FROM Billings
