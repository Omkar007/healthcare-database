USE master
GO
/****** Object:  Database HealthCare     ******/
IF DB_ID('HealthCareCenter') IS NOT NULL
	DROP DATABASE HealthCareCenter
GO

CREATE DATABASE HealthCareCenter
GO

USE HealthCareCenter
GO

/****** Object:  Table Employees    ******/
CREATE TABLE Employees(
	EmployeeID int NOT NULL,
	EmployeeFirstName varchar(50) NOT NULL,
	EmployeeMiddleName varchar(50) NOT NULL,
	EmployeeLastName varchar(50) NULL,
	EmployeeSSN varchar(10) NOT NULL,
	EmployeeEmail varchar(20) NOT NULL,
	EmployeePhone varchar(20) NULL,
	EmployeeOffice varchar(30) NOT NULL,
	EmployeeOfficePhone varchar(20) NOT NULL,
	EmployeeAddressID int NOT NULL,
	EmployeeWorkScheduleID int NOT NULL,
	EmployeeDepartmentID int NOT NULL,
	EmployeeCurrentJobTitle varchar(50) NOT NULL,
	EmployeePastJobTitle varchar(50) NULL,
	EmployeeContractType varchar(20) NOT NULL,
	EmployeeContractTerm varchar(10) NULL,
	EmployeeSalary varchar(15) NOT NULL

 CONSTRAINT PK_Employees PRIMARY KEY CLUSTERED(
	EmployeeID ASC
 )
) 
GO

/****** Object:  Table Benefits    ******/
CREATE TABLE Benefits(
	BenefitID int NOT NULL,
	BenefitName varchar(50) NOT NULL,
	BenefitDetails varchar(250) NULL

 CONSTRAINT PK_Benefits PRIMARY KEY CLUSTERED(
	BenefitID ASC
 )
)
GO

/****** Object:  Table EmployeeBenefits    ******/
CREATE TABLE EmployeeBenefits(
	BenefitID int NOT NULL,
	EmployeeID int NOT NULL

 CONSTRAINT PK_EmployeeBenefits PRIMARY KEY CLUSTERED(
	BenefitID,EmployeeID
 )
)
GO


/****** Object:  Table WorkSchedule    ******/
CREATE TABLE WorkSchedule(
	WorkScheduleID int NOT NULL,
	ShiftID int NOT NULL,
	RoomID int NOT NULL
 CONSTRAINT PK_WorkSchedules PRIMARY KEY CLUSTERED(
	WorkScheduleID ASC
 )
)
GO

/****** Object:  Table Department     ******/
CREATE TABLE Department(
	DepartmentID int NOT NULL,
	DepartmentName varchar(50) NOT NULL,
	DepartmentLocation varchar(50) NOT NULL
 CONSTRAINT PK_Department PRIMARY KEY CLUSTERED(
	DepartmentID ASC
 )
)
GO

/****** Object:  Table Shifts     ******/
CREATE TABLE Shifts(
	ShiftID int NOT NULL,
	ShiftDuration int NULL,
	ShiftStartTime time(0) NOT NULL,
	ShiftEndTime time(0) NOT NULL,
	ShiftType varchar(10) NULL

 CONSTRAINT PK_Shift PRIMARY KEY CLUSTERED(
	ShiftID ASC
 )
)
GO

/****** Object:  Table Reviews     ******/
CREATE TABLE Reviews(
	ReviewID int Identity(1,1) NOT NULL,
	Review varchar(150) NOT NULL,
	PatientID int NOT NULL,
	EmployeeID int NOT NULL,
	ReviewDate datetime NULL
 CONSTRAINT PK_Review PRIMARY KEY CLUSTERED(
	ReviewID
 )
)
GO

/****** Object:  Table Address     ******/
CREATE TABLE Address(
	AddressID int Identity(1,1) NOT NULL,
	AddressStreetAppartment varchar(200) NOT NULL,
	State varchar(50) NOT NULL,
	Country varchar(50) NOT NULL,
	ZipCode varchar(20) NOT NULL
 CONSTRAINT PK_Address PRIMARY KEY CLUSTERED(
	AddressID
 )
)
GO

/****** Object:  Table Insurance Coverage     ******/
CREATE TABLE InsuranceCoverage(
	InsuranceCoverageID int NOT NULL,
	CompanyName varchar(40) NOT NULL,
	CompanyPhone varchar(11) NOT NULL,
	CompanyContactInfo varchar(50) NOT NULL,
	AddressID int NULL,
	InsuranceCost Money NOT NULL,
	InsuranceDuration varchar(10) NOT NULL,
	AmountCoveredInfo decimal(5,2) NOT NULL,
	PlanCoverageInfo varchar(100),
	InsurancePlanID int NOT NULL
 CONSTRAINT PK_InsuranceCoverage PRIMARY KEY CLUSTERED(
	InsuranceCoverageID
 )
)
GO

/****** Object:  Table Insurance Plan     ******/
CREATE TABLE InsurancePlan(
	InsurancePlanID int NOT NULL,
	InsurancePlanName varchar(40) NOT NULL

 CONSTRAINT PK_InsurancePlan PRIMARY KEY CLUSTERED(
	InsurancePlanID
 )
)
GO

/****** Object:  Table Visitors    ******/
CREATE TABLE Visitors(
	VisitorID int NOT NULL,
	VisitorName varchar(40) NOT NULL,
	VisitorDateOfBirth Date NOT NULL,
	VisitorEnterTime datetime NOT NULL,
	VisitorExitTime datetime NOT NULL,
	VisitorPhoto Varbinary(max) NULL,
	PatientID int NOT NULL
 CONSTRAINT PK_Visitor PRIMARY KEY CLUSTERED(
	VisitorID
 )
)
GO

/****** Object:  Table Discharge    ******/
CREATE TABLE Discharge(
	DischargeID int NOT NULL,
	EmployeeID int NOT NULL,
	DischargeRemark varchar(100) NOT NULL,
	PatientID int NOT NULL
 CONSTRAINT PK_Discharge PRIMARY KEY CLUSTERED(
	DischargeID
 )
)
GO

/****** Object:  Table PatientRooms    ******/
CREATE TABLE PatientRooms(
	RoomID int NOT NULL,
	RoomType varchar(20) NOT NULL,
	RoomLocation varchar(40) NOT NULL,
	RoomCapacity varchar(20) NOT NULL,
	RoomStatus varchar(15) NULL,

 CONSTRAINT PK_PatientRooms PRIMARY KEY CLUSTERED(
	RoomID
 )
)
GO

/****** Object:  Table MedicalDevicesEquipments    ******/
CREATE TABLE MedicalDevicesEquipments(
	MedicalEquipmentID int NOT NULL,
	MedicalEquipmentName varchar(50) NOT NULL,
	MedicalEquipmentModelNo varchar(20) NULL,
	MedicalEquipmentCost Money NOT NULL CHECK(MedicalEquipmentCost > 0),
 CONSTRAINT PK_MedicalDevicesEquipments PRIMARY KEY CLUSTERED(
	MedicalEquipmentID
 )
)
GO

/****** Object:  Table RoomMedicalDevices    ******/
CREATE TABLE RoomMedicalDevices(
	RoomID int NOT NULL,
	MedicalEquipmentID int NOT NULL

 CONSTRAINT PK_RoomMedicalDevices PRIMARY KEY CLUSTERED(
	RoomID,
	MedicalEquipmentID
 )
)
GO

/****** Object:  Table RoomHistory    ******/
CREATE TABLE RoomScheduleHistory(
	RoomScheduleHistoryID int NOT NULL,
	RoomCapacity varchar(20) NOT NULL,
	RoomHistoryTillDate date NULL,
	RoomID int NOT NULL,
	Patients varchar(250) NULL
 CONSTRAINT PK_RoomScheduleHistory PRIMARY KEY CLUSTERED(
	RoomScheduleHistoryID
 )
)
GO

/****** Object:  Table Patients    ******/
CREATE TABLE Patients(
	PatientID int NOT NULL,
	PatientFirstName varchar(50) NOT NULL,
	PatientMiddleName varchar(50) NOT NULL,
	PatientLastName varchar(50) NULL,
	PatientSSN varchar(10) NULL,
	PatientEmail varchar(20) NOT NULL,
	PatientPhone varchar(20) NOT NULL,
	PatientSpouseFirstName varchar(50) NULL,
	PatientSpouseLastName varchar(50) NULL,
	InsuranceCoverageID int NULL,
	PatientAddressID int NOT NULL,
	PatientPrimaryDoctor int NOT NULL,
	PatientRoomID int NOT NULL

 CONSTRAINT PK_Patients PRIMARY KEY CLUSTERED(
	PatientID
 )
)
GO


/****** Object:  Table Diagnosis    ******/
CREATE TABLE Diagnosis(
	DiagnosisID int NOT NULL,
	BloodSugarTest varchar(200) NULL,
	ECG varchar(100) NULL,
	NuclearMedicineTest varchar(50) NULL,
	XRayTest varchar(50) NULL,
	Symptoms varchar(50) NOT NULL,
	DiagnosisDate datetime NOT NULL,
	PatientID int NOT NULL

 CONSTRAINT PK_Diagnosis PRIMARY KEY CLUSTERED(
	DiagnosisID
 )
)
GO

/****** Object:  Table PatientHistory    ******/
CREATE TABLE PatientHistory(
	PatientHistoryID int Identity(1,1) NOT NULL,
	PatientWeight varchar(10) NULL,
	PatientHeight varchar(10) NULL,
	PatientAge int NOT NULL,
	HeartRate varchar(30) NOT NULL,
	BloodPressure varchar(30) NOT NULL,
	RespiratoryRate varchar(30) NOT NULL,
	PatientCheckIn datetime NULL,
	PatientCheckOut datetime NULL,
	DiagnosisID int NULL,
	PatientReferalDoctor int NULL,
	PatientPhysian int NOT NULL,
	PatientID int NOT NULL,
	PatientMedicationInfo varchar(250) NULL,
	PatientProcedures varchar(250) NULL,
	HospitalizationInfoID int NULL,
	DischargeID int NULL

 CONSTRAINT PK_PatientHistory PRIMARY KEY CLUSTERED(
	PatientHistoryID
 )
)
GO 

/****** Object:  Table Medication    ******/
CREATE TABLE Medication(
	MedicationID int NOT NULL,
	MedicationName varchar(50) NOT NULL,
	RecommendedDose varchar(30) NULL,
	MedicationPurpose varchar(50) NULL,
	MedicationCost Money NOT NULL

 CONSTRAINT PK_Medication PRIMARY KEY CLUSTERED(
	MedicationID
 )
)
GO

/****** Object:  Table PatientMedicationDetails ******/
CREATE TABLE PatientMedicationDetails(
	MedicationID int NOT NULL,
	PatientID int NOT NULL

 CONSTRAINT PK_PatientMedicationDetails PRIMARY KEY CLUSTERED(
	MedicationID,PatientID
 )
)
GO

/****** Object:  Table PatientProcedureDetails ******/
CREATE TABLE PatientProceduresDetails(
	ProcedureID int NOT NULL,
	PatientID int NOT NULL

 CONSTRAINT PK_PatientProceduresDetails PRIMARY KEY CLUSTERED(
	ProcedureID,PatientID
 )
)
GO

/****** Object:  Table Procedure    ******/
CREATE TABLE Procedures(
	ProcedureID int NOT NULL,
	ProcedureName varchar(50) NOT NULL,
	ProcedureCost Money NOT NULL CHECK(ProcedureCost > 0),
	ProcedureType varchar(50) NULL

 CONSTRAINT PK_Procedures PRIMARY KEY CLUSTERED(
	ProcedureID
 )
)
GO

/****** Object:  Table Billing    ******/
CREATE TABLE Billings(
	BillingID int NOT NULL,
	Visits varchar(10) NULL,
	PaymentMethod varchar(50) NOT NULL,
	Payor varchar(50) NOT NULL,
	items int NOT NULL,
	TotalCost Money NOT NULL CHECK(TotalCost > 0),
	PatientID int NOT NULL

 CONSTRAINT PK_Billings PRIMARY KEY CLUSTERED(
	BillingID
 )
)
GO

/****** Foreign Key Creation   ******/

ALTER TABLE Reviews  
WITH NOCHECK 
ADD CONSTRAINT FK_Reviews_Employee
FOREIGN KEY(EmployeeID)
REFERENCES Employees (EmployeeID)
ON DELETE CASCADE
GO

ALTER TABLE Reviews  
WITH NOCHECK 
ADD CONSTRAINT FK_Reviews_Patient
FOREIGN KEY(PatientID)
REFERENCES Patients (PatientID)
ON DELETE CASCADE
GO

ALTER TABLE Employees  
WITH NOCHECK 
ADD CONSTRAINT FK_Employees_Department
FOREIGN KEY(EmployeeDepartmentID)
REFERENCES Department (DepartmentID)
GO

ALTER TABLE Employees  
WITH NOCHECK 
ADD CONSTRAINT FK_Employees_Address
FOREIGN KEY(EmployeeAddressID)
REFERENCES Address (AddressID)
GO

ALTER TABLE Employees  
WITH NOCHECK 
ADD CONSTRAINT FK_Employees_WorkSchedule
FOREIGN KEY(EmployeeWorkScheduleID)
REFERENCES WorkSchedule (WorkScheduleID)
GO

ALTER TABLE WorkSchedule  
WITH NOCHECK 
ADD CONSTRAINT FK_WorkSchedule_Room
FOREIGN KEY(RoomID)
REFERENCES PatientRooms (RoomID)
GO

ALTER TABLE Visitors  
WITH NOCHECK 
ADD CONSTRAINT FK_Visitors_Patient
FOREIGN KEY(PatientID)
REFERENCES Patients (PatientID)
GO

ALTER TABLE Discharge  
WITH NOCHECK 
ADD CONSTRAINT FK_Discharge_Employee
FOREIGN KEY(EmployeeID)
REFERENCES Employees (EmployeeID)
GO

ALTER TABLE Discharge  
WITH NOCHECK 
ADD CONSTRAINT FK_Discharge_Patient
FOREIGN KEY(PatientID)
REFERENCES Patients (PatientID)
GO

ALTER TABLE RoomMedicalDevices  
WITH NOCHECK 
ADD CONSTRAINT FK_MedicalDevices
FOREIGN KEY(RoomID)
REFERENCES PatientRooms (RoomID)
GO

ALTER TABLE RoomMedicalDevices  
WITH NOCHECK 
ADD CONSTRAINT FK_MedicalDevicesInRoom
FOREIGN KEY(MedicalEquipmentID)
REFERENCES MedicalDevicesEquipments (MedicalEquipmentID)
GO

ALTER TABLE RoomScheduleHistory  
WITH NOCHECK 
ADD CONSTRAINT FK_RoomScheduleHistory_Room
FOREIGN KEY(RoomID)
REFERENCES PatientRooms (RoomID)
GO

ALTER TABLE Patients  
WITH NOCHECK 
ADD CONSTRAINT FK_Patients_Address
FOREIGN KEY(PatientAddressID)
REFERENCES Address (AddressID)
GO

ALTER TABLE Patients  
WITH NOCHECK 
ADD CONSTRAINT FK_Patients_Rooms
FOREIGN KEY(PatientRoomID)
REFERENCES PatientRooms (RoomID)
GO

ALTER TABLE Patients  
WITH NOCHECK 
ADD CONSTRAINT FK_Patients_Employees
FOREIGN KEY(PatientPrimaryDoctor)
REFERENCES Employees (EmployeeID)
GO

ALTER TABLE Patients  
WITH NOCHECK 
ADD CONSTRAINT FK_Patients_Insurance
FOREIGN KEY(InsuranceCoverageID)
REFERENCES InsuranceCoverage (InsuranceCoverageID)
GO

ALTER TABLE Diagnosis 
WITH NOCHECK 
ADD CONSTRAINT FK_Diagnosis_Patient
FOREIGN KEY(PatientID)
REFERENCES Patients (PatientID)
GO

ALTER TABLE InsuranceCoverage 
WITH NOCHECK 
ADD CONSTRAINT FK_InsuranceCoverage
FOREIGN KEY(InsurancePlanID)
REFERENCES InsurancePlan (InsurancePlanID)
GO

ALTER TABLE EmployeeBenefits 
WITH NOCHECK 
ADD CONSTRAINT FK_EmployeeID
FOREIGN KEY(EmployeeID)
REFERENCES Employees (EmployeeID)
GO

ALTER TABLE EmployeeBenefits 
WITH NOCHECK 
ADD CONSTRAINT FK_BenefitID
FOREIGN KEY(BenefitID)
REFERENCES Benefits (BenefitID)
GO

ALTER TABLE Billings 
WITH NOCHECK 
ADD CONSTRAINT FK_Patient
FOREIGN KEY(PatientID)
REFERENCES Patients (PatientID)
GO

ALTER TABLE PatientHistory 
WITH NOCHECK 
ADD CONSTRAINT FK_PatientHistory
FOREIGN KEY(PatientID)
REFERENCES Patients (PatientID)
GO

ALTER TABLE PatientHistory 
WITH NOCHECK 
ADD CONSTRAINT FK_PhysianInfo
FOREIGN KEY(PatientPhysian)
REFERENCES Employees (EmployeeID)
GO

ALTER TABLE PatientMedicationDetails 
WITH NOCHECK 
ADD CONSTRAINT FK_Medication
FOREIGN KEY(MedicationID)
REFERENCES Medication (MedicationID)
GO

ALTER TABLE PatientMedicationDetails 
WITH NOCHECK 
ADD CONSTRAINT FK_MedicationPatient
FOREIGN KEY(PatientID)
REFERENCES Patients (PatientID)
GO

ALTER TABLE PatientProceduresDetails 
WITH NOCHECK 
ADD CONSTRAINT FK_Procedure
FOREIGN KEY(ProcedureID)
REFERENCES Procedures (ProcedureID)
GO

ALTER TABLE PatientProceduresDetails 
WITH NOCHECK 
ADD CONSTRAINT FK_ProcedurePatient
FOREIGN KEY(PatientID)
REFERENCES Patients (PatientID)
GO

ALTER TABLE InsuranceCoverage 
WITH NOCHECK 
ADD CONSTRAINT FK_CompanyAddress
FOREIGN KEY(AddressID)
REFERENCES Address (AddressID)
GO

ALTER TABLE WorkSchedule 
WITH NOCHECK 
ADD CONSTRAINT FK_Shifts
FOREIGN KEY(ShiftID)
REFERENCES Shifts (ShiftID)
GO

/****** Unclustered Indexes creation   ******/

 CREATE NONCLUSTERED INDEX PatientHistory_PatienID_IDX ON PatientHistory(PatientID);

 CREATE NONCLUSTERED INDEX Reviews_PatientID_IDX ON Reviews(PatientID);

 CREATE NONCLUSTERED INDEX Reviews_EmployeeID_IDX ON Reviews(EmployeeID);

 CREATE NONCLUSTERED INDEX Billings_PatientID_IDX ON Billings(PatientID);

 CREATE NONCLUSTERED INDEX Visitors_PatientID_IDX ON Visitors(PatientID);

 CREATE NONCLUSTERED INDEX RoomScheduleHistory_Room_IDX ON RoomScheduleHistory(RoomScheduleHistoryID);

 CREATE NONCLUSTERED INDEX Diagnosis_Patient_IDX ON Diagnosis(PatientID);
